import static spark.Spark.get;
import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import java.util.HashMap;

public class App {

    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new VelocityTemplateEngine(configuredEngine);

        get("/latihan1", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String bilString1 = req.queryParamOrDefault("bil1", "0");
            String bilString2 = req.queryParamOrDefault("bil2", "0");
            int bilInput1 = Integer.parseInt(bilString1);
            int bilInput2 = Integer.parseInt(bilString2);
            LatihanOne lat = new LatihanOne();
            model.put("resultAdd", lat.add(bilInput1,bilInput2));
            model.put("resultRemove", lat.remove(bilInput1,bilInput2));
            model.put("bilInput1", bilInput1);
            model.put("bilInput2", bilInput2);
            String templatePath = "/view/index.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/latihan2", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String bilString1 = req.queryParamOrDefault("bil1","0");
            String bilString2 = req.queryParamOrDefault("bil2","0");
            int bilInput1 = Integer.parseInt(bilString1);
            int bilInput2 = Integer.parseInt(bilString2);
            LatihanTwo l = new LatihanTwo();
            model.put("result", l.getPoint(bilInput1,bilInput2));
            String templatePath = "/view/nestedclass.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/latihan3", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String a = "Saya sedang belajar KPL";
            String b = a;
            model.put("a", a);
            model.put("b", b);
            model.put("result1", a.equals(b));
            model.put("result2", a == b);
            String c = new String(a);
            model.put("c", c);
            model.put("result3", a.equals(c));
            model.put("result4", a == c);
            String templatePath = "/view/comparename.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/latihan4", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            User u = new User("Abdi",12);
            model.put("result",u.toString());
            String templatePath = "/view/latihan4.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/latihan5", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String bilString1 = req.queryParamOrDefault("bil1","0");
            String bilString2 = req.queryParamOrDefault("bil2","0");
            int bilInput1 = Integer.parseInt(bilString1);
            int bilInput2 = Integer.parseInt(bilString2);
            LatihanFive lat = new LatihanFive();
            model.put("result", lat.getAbsAdd(bilInput1,bilInput2));
            String templatePath = "/view/validate.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/latihan6", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            SuperClass bd = new SuperClass();
            SuperClass sc = new SubClass();
            model.put("result1", bd.doLogic());
            model.put("result2", sc.doLogic());
            String templatePath = "/view/Override.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });
    }

}
