public class LatihanTwo {
    private int x;
    private int y;
    // mengubah dari public ke private
    private class Point {
        private String getPoint(int x, int y) {
            return "(" + x + ", " + y + ")";
        }
    }

    public String getPoint(int x, int y){
        return new Point().getPoint(x,y);
    }
}
